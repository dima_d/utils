package com.utils;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Editable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.text.style.ReplacementSpan;
import android.util.Log;
import android.view.View;

import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.Locale;

/**
 * Created by Dmitry.Subbotenko on 20.01.2016.
 */
public class PhoneNumberFormatter implements TransformationMethod, TextWatcher {

  private static final String TAG = PhoneNumberFormatter.class.getSimpleName();
  private AsYouTypeFormatter mFormatter;

  public PhoneNumberFormatter() {
    this.mFormatter = PhoneNumberUtil.getInstance()
        .getAsYouTypeFormatter(Locale.getDefault().getCountry());
  }

  @Override
  public CharSequence getTransformation(CharSequence source, View view) {
    String formated = format(source);

    if (source instanceof Spannable) {
      setSpans((Spannable) source, formated);
      return source;
    }

    return formated;
  }

  @Override
  public void onFocusChanged(View view, CharSequence sourceText, boolean focused, int direction,
      Rect previouslyFocusedRect) {

  }

  @Override
  public void beforeTextChanged(CharSequence source, int start, int count, int after) {

  }

  @Override
  public void onTextChanged(CharSequence source, int start, int before, int count) {
    if (source instanceof Spannable) {
      setSpans((Spannable) source, format(source));
    }
  }

  private String setSpans(Spannable spannable, String formated ) {

    CharterSpan[] vr = spannable.getSpans(0, spannable.length(),
        CharterSpan.class);
    for (int i = 0; i < vr.length; i++) {
      spannable.removeSpan(vr[i]);
    }

    //      LeadingMarginSpan span = new LeadingMarginSpan.Standard(4);

    int sourceIter = 0;
    for (int formatedIter = 0; formatedIter < formated.length(); formatedIter++) {
      if (Character.isDigit(formated.charAt(formatedIter))) {
        sourceIter++;
        continue;
      }

      Log.d(TAG, "setSpan " + sourceIter + " " + formatedIter);
      spannable.setSpan(new CharterSpan(formated.charAt(formatedIter)), sourceIter, sourceIter + 1,
          Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    return formated;
  }

  private String format(CharSequence spannable) {
    mFormatter.clear();
    String formated = "";
    for (int i = 0; i < spannable.length(); i++) {
      formated = mFormatter.inputDigit(spannable.charAt(i));
    }
    Log.d(TAG, "formated " + formated);
    return formated;
  }

  public class CharterSpan extends ReplacementSpan {

    private String charters;

    public CharterSpan(char ch) {
      this(String.valueOf(ch));
    }

    public CharterSpan(String charters) {
      this.charters = charters;
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end,
        Paint.FontMetricsInt fm) {

      int space = Math.round(paint.measureText(charters, 0, charters.length()));

      return Math.round(paint.measureText(text, start, end)) + space;
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y,
        int bottom, Paint paint) {

      int space = Math.round(paint.measureText(charters, 0, charters.length()));

      canvas.drawText(text, start, end, x + space, y, paint);

      canvas.drawText(charters, x, y, paint);

    }
  }

  @Override
  public void afterTextChanged(Editable source) {

  }
}
