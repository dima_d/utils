package com.tassta.flex.features.video_call.state_mashine

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

interface Consumer<in T> {
    fun accept(t: T)
}

interface SessionEvent {

}


abstract class  SessionState {
    val map = ConcurrentHashMap<KClass<out SessionEvent>, (t:Any) -> Unit>()
    var onSet : (() -> Unit)? = null

    fun <T : SessionEvent> event(clazz: KClass<T>, consumer: (t:T) -> Unit) = map.put(clazz, consumer as (t: Any) -> Unit)
    fun <T : SessionEvent>fire(event:T) { (map[event::class] as? (t:T) -> Unit)?.invoke(event)}
}

interface IStateMachine {
    fun stop()
    fun fire(event: SessionEvent)
}

abstract class StateMachine : IStateMachine {
    private val events = PublishSubject.create<SessionEvent>()
    private val states = BehaviorSubject.create<SessionState>()
    private val compositeDisposable = CompositeDisposable()

    init {
        compositeDisposable.add(events.observeOn(Schedulers.io()).subscribe{ states.value?.fire(it) })
        compositeDisposable.add(states.observeOn(AndroidSchedulers.mainThread()).subscribe{ it.onSet?.invoke()})
    }

    override fun stop(){
        compositeDisposable.dispose()
    }

    protected fun state(state:SessionState) = states.onNext(state)
    override fun fire(event: SessionEvent) = events.onNext(event)
}