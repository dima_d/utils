package com.tassta.flex.features.video_call.presenter

import com.tassta.flex.features.video_call.model.IVideoCallInteractor
import com.tassta.flex.features.video_call.presenter.IVideoCallStateMachine.*
import com.tassta.flex.features.video_call.state_mashine.IStateMachine
import com.tassta.flex.features.video_call.state_mashine.SessionEvent
import com.tassta.flex.features.video_call.state_mashine.SessionState
import com.tassta.flex.features.video_call.state_mashine.StateMachine
import com.tassta.flex.service.model.User
import com.tassta.flex.utils.ice.Ack
import com.tassta.flex.utils.ice.Invite


interface IVideoCallStateMachine : IStateMachine {

    class EventUserStart : SessionEvent
    class EventUserCancel : SessionEvent
    class EventRinging : SessionEvent
    class EventCancel : SessionEvent
    class EventAck : SessionEvent

    data class ActionUserStart(val user: User) : SessionEvent
    class ActionUserOk : SessionEvent
    class ActionUserCancel : SessionEvent
    data class ActionInvite(val invite: Invite) : SessionEvent
    class ActionRinging : SessionEvent
    class ActionCancel : SessionEvent
    data class ActionAck(val ack: Ack) : SessionEvent
    class ActionOk : SessionEvent

}

class VideoCallStateMachine(val presenter: IVideoCallPresenter, val interactor: IVideoCallInteractor) : StateMachine(), IVideoCallStateMachine {

    inner class SessionFinished : SessionState() {
        init {
            onSet = { presenter.stateFinished() }
            event(EventRinging::class) { state(SessionIncomingCall()) }
            event(EventUserStart::class) { state(SessionOutgoingCall()) }

            event(ActionUserStart::class){ interactor.startCall(it) }
            event(ActionInvite::class){ interactor.incomingCall(it) }
            event(ActionRinging::class){ interactor.ringingReceived() }
        }
    }

    inner class SessionStarted : SessionState() {
        init {
            onSet = { presenter.stateStarted() }
            event(EventUserCancel::class) { state(SessionFinished()) }
            event(EventCancel::class) { state(SessionFinished()) }

            event(ActionUserCancel::class){ interactor.cancelCall() }
            event(ActionCancel::class){ interactor.stopVideo() }
        }
    }

    inner class SessionIncomingCall : SessionState() {
        init {
            onSet = { presenter.stateRinging() }
            event(EventUserCancel::class) { state(SessionFinished()) }
            event(EventUserStart::class) { state(SessionStarted()) }

            event(ActionUserCancel::class){ interactor.cancelCall() }
            event(ActionCancel::class){ interactor.stopCall() }
            event(ActionUserOk::class){ interactor.sendAck() }
            event(ActionOk::class){ interactor.startVideo() }
        }
    }

    inner class SessionOutgoingCall : SessionState() {
        init {
            onSet = { presenter.stateOutgoingCall() }
            event(EventUserCancel::class) { state(SessionFinished()) }
            event(EventAck::class) { state(SessionStarted()) }

            event(ActionUserCancel::class){ interactor.cancelCall() }
            event(ActionCancel::class){ interactor.stopCall() }
            event(ActionAck::class){ interactor.ackRequested(it) }
        }
    }

    init {
        state(SessionFinished())
    }

}




