package com.tassta.flex.features.inhouse.customViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.tassta.flex.R;
import com.tassta.flex.features.inhouse.model.Point;
import com.tassta.flex.service.model.User;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Map view. Can show map from file, and show user position points. <br/>
 * Supports the draging and scaling map.<br/>
 * <br/>
 * <b>NB:</b> I use canvas scaling to change card scale.<br/>
 * If uou want use vector image you need to change onDraw method.<br/>
 * <br/>
 * Created by dmitry subbotenko on 11.04.2017.
 */

public class InHouseViewMap extends View {
    private static final String TAG = InHouseViewMap.class.getSimpleName();

    /**
     * Map scale in points per meter.
     */
    private static final float RESOLUTION = 10f;
    private static final int TEXT_SIZE = 15;

    private final float CROSS_SIZE;

    private final Paint mPaint;
    private final int otherColor;
    private final Bitmap myIcon;
    private final Bitmap otherUsersIcon;

    private Bitmap bitmap;
    private Point position;

    private float dragedPositionX;
    private float dragedPositionY;

    private ScaleGestureDetector mScaleDetector;
    private float scaleFactor = 1f;

    private GestureDetectorCompat gestureDetector;

    private final Map<User, Point> positions;


    public InHouseViewMap(@NonNull Context context) {
        this(context, null);
    }

    public InHouseViewMap(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InHouseViewMap(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mPaint = new Paint();

        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        gestureDetector = new GestureDetectorCompat(context, new DraggingListener());

        otherColor = getResources().getColor(R.color.inhouse_pointer_color2);

        myIcon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.map_pin_my);

        otherUsersIcon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.map_pin);


        mPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE, context.getResources().getDisplayMetrics()));

        positions = new HashMap<>();

        CROSS_SIZE = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics());
    }

    /**
     * Set map bitmap.
     * @param bitmap
     */
    public void setImageBitmap(@Nullable Bitmap bitmap) {
        this.bitmap = bitmap;
        invalidate();
    }

    /**
     * Add position for other user.
     * @param user
     * @param position
     */
    public void addUserPosition(User user, Point position){
        positions.put(user, position);
        invalidate();
    }

    /**
     * Add positions from Map<<>>.
     * @param pos
     */
    public void addAllUserPosition(Map<User, Point> pos){
        positions.putAll(pos);
        invalidate();
    }

    /**
     * Set my current position.
     * @param position
     */
    public void setPosition(Point position) {
        this.position = position;
        invalidate();
    }

    /**
     * Center map on my position
     */
    public void centerOnMe(){
        if (bitmap!=null){
            dragedPositionX = (getMeasuredWidth()/2)/scaleFactor - position.xPos * RESOLUTION;
            dragedPositionY = (getMeasuredHeight()/2)/scaleFactor + position.yPos * RESOLUTION - bitmap.getHeight();

            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (bitmap!=null){

            canvas.scale(scaleFactor, scaleFactor);
            canvas.drawBitmap(bitmap, dragedPositionX, dragedPositionY, null);

            canvas.scale(1/scaleFactor, 1/scaleFactor);
            if (position!=null) {
                drawPosition(canvas, position, "", myIcon, otherColor);
            }

            for(Iterator<User> iterator = positions.keySet().iterator();iterator.hasNext();){
                User user = iterator.next();
                drawPosition(canvas, positions.get(user), user.getName(), otherUsersIcon, otherColor);
            }

            drawCenterCross(canvas);

        }
    }

    private void drawCenterCross(Canvas canvas) {
        int centerX = canvas.getWidth() / 2;
        int centerY = canvas.getHeight() / 2;

        mPaint.setColor(otherColor);
        mPaint.setStrokeWidth(5);

        canvas.drawLine(centerX-CROSS_SIZE, centerY, centerX+CROSS_SIZE, centerY, mPaint);
        canvas.drawLine(centerX, centerY-CROSS_SIZE, centerX, centerY+CROSS_SIZE, mPaint);
    }

    private void drawPosition(Canvas canvas, Point position, String name, Bitmap recource, int textColor){
        float cx = (position.xPos * RESOLUTION + dragedPositionX)*scaleFactor - recource.getWidth()/2;
        float cy = (bitmap.getHeight() + dragedPositionY - position.yPos * RESOLUTION)*scaleFactor - recource.getHeight();

        canvas.drawBitmap(recource, cx, cy, null);
        mPaint.setColor(textColor);
        canvas.drawText(name, cx + 20, cy, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (bitmap == null)
                return false;

            scaleFactor *= detector.getScaleFactor();
            scaleFactor = Math.max(0.5f, Math.min(scaleFactor, 5.0f));

            invalidate();
            return true;
        }
    }

    private class DraggingListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (bitmap == null)
                return false;

            dragedPositionX-=distanceX/scaleFactor;
            dragedPositionY-=distanceY/scaleFactor;

            dragedPositionX = Math.max(-bitmap.getWidth(), Math.min(dragedPositionX, bitmap.getWidth()));
            dragedPositionY = Math.max(-bitmap.getHeight(), Math.min(dragedPositionY, bitmap.getHeight()));

            invalidate();
            return true;
        }
    }
}
